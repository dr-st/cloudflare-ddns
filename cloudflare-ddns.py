#!/usr/bin/env python3

import argparse
import datetime
import ipaddress
import json
import requests
import socket
import tempfile
import os
import sys
import traceback

def build_zones(zones, record_type, lst):
    if lst is not None:
        for name in lst:
            zone = str.join('.', name.split('.')[-2:])
            if zone not in zones:
                zones[zone] = []
            zones[zone].append({'type': record_type, 'name': name})

parser = argparse.ArgumentParser(description='Update cloudflare DNS records for dynamic IPs. Caches cloudflare dns status to tempfile.')
parser.add_argument('--A', '-4', action='append', help='The A record names to update (e.g. www.example.com)')
parser.add_argument('--AAAA', '-6', action='append', help='The AAAA record names to update (e.g. www.example.com)')
parser.add_argument('--verbose', '-v', action='store_true', help='Verbose mode')
parser.add_argument('token', nargs=1, help='The cloudflare API token (must have \"Zone.Zone\" and \"Zone.DNS\" permissions for \"All zones\")')
args = parser.parse_args()

IP_SERVICE = 'https://icanhazip.com'
CF = 'https://api.cloudflare.com/client/v4'
HEADERS = { 'Authorization': 'Bearer {}'.format(args.token[0]), 'Content-Type': 'application/json' }

zones = {}
build_zones(zones, 'A', args.A)
build_zones(zones, 'AAAA', args.AAAA)

def print_verbose(*a):
    if args.verbose != False:
        sys.stdout.write('[{}] '.format(datetime.datetime.now()))
        print(*a)

def except_verbose():
    if args.verbose != False:
        traceback.print_exc()

def print_always(*a):
    sys.stdout.write('[{}] '.format(datetime.datetime.now()))
    print(*a)

def cloudflare_api(method, url, **kwargs):
    r = requests.request(method, CF + url, headers=HEADERS, **kwargs).json()
    if r['success'] != True:
        raise Exception('Cloudflare API reponse contains error: {}'.format(r))
    return r

def get_zone_id(zone):
    print_verbose('Requesting zone ID for {}'.format(zone))
    r = cloudflare_api('GET', '/zones?name={}'.format(zone))
    if len(r['result']) != 1:
        raise Exception('Cloudflare API reponse contains unexpected numer of results: {}'.format(r))
    zone_id = r['result'][0]['id']
    print_verbose('Zone ID for {}: {}'.format(zone, zone_id))
    return zone_id

def update_record(zone_id, record_type, name, new_content):
    print_verbose('Requesting type {} DNS records for {} in zone with ID {}'.format(record_type, name, zone_id))
    r = cloudflare_api('GET', '/zones/{}/dns_records?type={}&name={}'.format(zone_id, record_type, name))
    if len(r['result']) != 1:
        raise Exception('Cloudflare API reponse contains unexpected numer of results: {}'.format(r))
    r = r['result'][0]
    old_content = r['content']
    print_verbose('Type {} record {} with ID {} contains {}'.format(record_type, r['id'], name, old_content))
    if old_content != new_content:
        print_verbose('Updating type {} record {} to content {}'.format(record_type, name, new_content))
        data = '{{"content": "{}"}}'.format(new_content)
        r = cloudflare_api('PATCH', '/zones/{}/dns_records/{}'.format(zone_id, r['id']), data=data)
        print_always('Updated record {} from {} to {}'.format(name, old_content, new_content))

def request_family(family, *args):
    old = requests.packages.urllib3.util.connection.allowed_gai_family
    requests.packages.urllib3.util.connection.allowed_gai_family = lambda : family
    try:
        r = requests.get(*args)
    finally:
        requests.packages.urllib3.util.connection.allowed_gai_family = old
    return r

def get_ip(family):
    address_type = 'IPv4' if family == 'A' else 'IPv6'
    print_verbose('Fetching public {} address'.format(address_type))
    try:
        r = request_family(socket.AF_INET if family == 'A' else socket.AF_INET6, IP_SERVICE)
        ip = ipaddress.IPv4Address(r.text.strip()) if family == 'A' else ipaddress.IPv6Address(r.text.strip())
        if ip.is_global == False:
            raise Exception('{} address `{}` is not a valid global {} address, aborting'.format(address_type, r.text.strip(), address_type))
        print_verbose('Public {} address is {}'.format(address_type, ip))
        return str(ip)
    except Exception as e:
        print_verbose('Could not get {}, reason:'.format(address_type))
        except_verbose()
        return ''

def write_cache(record_type, record_name, new_content):
    try:
        try:
            with open(os.path.join(tempfile.gettempdir(), 'cloudflare-ddns-cache'), 'r') as cache_file:
                data = json.loads(cache_file.read())
        except:
            data = {}
        if record_name not in data:
            data[record_name] = {}
        data[record_name][record_type] = new_content
        with open(os.path.join(tempfile.gettempdir(), 'cloudflare-ddns-cache'), 'w') as cache_file:
            json.dump(data, cache_file)
    except:
        print_verbose('Cannot write to cache')
        except_verbose()

def read_cache(record_type, record_name):
    try:
        with open(os.path.join(tempfile.gettempdir(), 'cloudflare-ddns-cache'), 'r') as cache_file:
            data = json.loads(cache_file.read())
            return data[record_name][record_type]
    except:
        return ''

new_ip = {}
new_ip['A'] = get_ip('A') if len(args.A) > 0 else ''
new_ip['AAAA'] = get_ip('AAAA') if len(args.AAAA) > 0 else ''

for zone_name, records in zones.items():
    zone_id = get_zone_id(zone_name)
    for record in records:
        new_content = new_ip[record['type']]
        if new_content != '':
            cached_content = read_cache(record['type'], record['name'])
            if cached_content != new_content:
                try:
                    update_record(zone_id, record['type'], record['name'], new_content)
                    write_cache(record['type'], record['name'], new_content)
                except Exception as e:
                    print_always('Unable to update {} record {}, reason:'.format(record['type'], record['name']))
                    traceback.print_exc()
            else:
                print_verbose('Not updating type {} record {}, matches cached value'.format(record['type'], record['name']))
