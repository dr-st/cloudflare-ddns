# Cloudflare DDNS

Update cloudflare DNS A and AAAA records based on perceived public IP.

The public IP is fetched from https://icanhazip.com

## requirements

Requires a cloudflare token Zone.Zone and Zone.DNS permissions for All zones.

Requires python 3 (tested on python 3.7.3 on debian), probably works on any OS that runs python 3.

## usage

Run with --help for info on the command-line options, schedule to run periodically with cron or something similar.

## example

Update both type A and type AAAA records for www.example.com

cloudflare-ddns.py -4 www.example.com -6 www.example.com -v <token>

# license

MIT
